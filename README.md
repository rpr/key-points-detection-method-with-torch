# keyPointsDetectionMethodWithTorch

#### 介绍
物体关键点检测算法。
关键点的数量可以随意指定，但训练集需要与指定的关键点数量一致。
本算法是基于Resnet+dsntnn组合网络构成的关键点坐标回归算法--KPDEM_model.
仓库的文件源码很简单。一个模型文件，两个数据处理文件（训练集和测试集），一个训练文件和一个测试文件。以及readme文件。
1. 模型设计流程文件（模型文件：`keypoints_Net.py`）模型设计流程介绍参见blog；
2. 数据集读取文件：测试数据集文件对应:`data_test_fashion_hw.py`，训练数据集文件对应：`data_process_fashion_hw.py`
3. 训练文件：`train_KPDEM_model.py`
4. 测试文件：`test_KPDM_model.py`
5. 训练集和测试集数据：链接：https://pan.baidu.com/s/1vXL-FCt8lehQrO6FVvDQ1w 
提取码：u4mk 

#### 算法架构
结构和流程说明
详细见blog:https://blog.csdn.net/beauthy/article/details/114318277
需要的数据资源，可以参见blog介绍。


#### 安装库说明

1.  pytorch 1.5
2.  dnstnn
3.  collections

#### 使用说明

1.  数据预处理，或数据集准备；
    数据路径csv_file,root_dir传入：
    `dressDataset = TestDataSet(csv_file=r"E:\Datasets\Fashion\Fashion AI-keypoints\test\test_dress.csv",
                           root_dir=r"E:\Datasets\Fashion\Fashion AI-keypoints\test",
                           num=15,
                           transforms_img=transform_img,
                           )`
    训练数据集略有不同。
2.  模型训练和保存模型
    运行`train_KPDEM_model.py`，通过设置`model = CoordRegression(n_locations=13)`的`n_locations`对应物体的关键点数量。
    设置`optimizer = optim.RMSprop(model.parameters(), lr=2e-4, alpha=0.85)`的lr，alpha的值，调节超参数，帮助收敛损失函数，优化模型。
    训练完成，保存模型：`torch.save(model, r'D:/TanHaiyan/Models/KPDEM/trouser_kp/' + 'blouse_kp' + str(epoch) + '.pth')`,设置保存文件的名称和位置。
    
3.  模型测试和验证
    运行`test_KPDM_model.py`文件，`model = torch.load(path)`，`path`对应模型文件地址.
    注意，需要将模型文件`keypoints_Net.py`与测试文件`test_KPDM_model.py`放在同一个路径，否则，torch.load(path)，会报错。



